# Changelog

<!--next-version-placeholder-->

## v0.3.2 (2021-11-25)
### Fix
* Test fixing2 ([`48aef24`](https://gitlab.com/eddienero/mypkg/-/commit/48aef244b0f05f96e15d064abc70585138cd5018))

## v0.3.1 (2021-11-25)
### Fix
* Test fixing ([`bb143c8`](https://gitlab.com/eddienero/mypkg/-/commit/bb143c88b8589ac546681a9ec12d973e84904754))

## v0.3.0 (2021-11-25)
### Feature
* Add CI/CD workflow ([`780f297`](https://gitlab.com/eddienero/mypkg/-/commit/780f297aa881218783f5c22a2054496256690f5c))

## v0.2.0 (23/11/2021)

### Feature

- Added new datasets modules to load example data

### Fix

- Check type of argument passed to `plotting.plot_words()`

### Tests

- Added new tests to all package modules in test_pycounts.py

## v0.1.0 (24/08/2021)

- First release of `mypkg_edne`
