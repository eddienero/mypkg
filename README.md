# mypkg_edne

Calculate word counts in a text file! Test

## Installation

```bash
$ pip install mypkg_edne
```

## Usage

`mypkg_edne` can be used to count words in a text file and plot the results
as follows:

```python
from mypkg_edne.mypkg_edne import count_words
from mypkg_edne.plotting import plot_words
import matplotlib.pyplot as plt

file_path = "test.txt"  # path to your file
counts = count_words(file_path)
fig = plot_words(counts, n=10)
plt.show()
```

## Contributing

Interested in contributing? Check out the contributing guidelines. 
Please note that this project is released with a Code of Conduct. 
By contributing to this project, you agree to abide by its terms.

## License

`mypkg-edne` was created by Mikhail Musolin. It is licensed under the terms
of the MIT license.

## Credits

`mypkg-edne` was created with 
[`cookiecutter`](https://cookiecutter.readthedocs.io/en/latest/) and 
the `py-pkgs-cookiecutter` 
[template](https://github.com/py-pkgs/py-pkgs-cookiecutter).
