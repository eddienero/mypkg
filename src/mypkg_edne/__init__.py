# read version from installed package
from importlib.metadata import version
__version__ = version("mypkg_edne")

# populate package namespace
from mypkg_edne.mypkg_edne import count_words
from mypkg_edne.plotting import plot_words
